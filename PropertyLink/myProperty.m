//
//  myProperty.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 14/01/2014.
//  Copyright (c) 2014 SCube. All rights reserved.
//

#import "myProperty.h"
#import <QuartzCore/QuartzCore.h>
@interface myProperty ()

@end

@implementation myProperty
@synthesize theView;
@synthesize firstName,Address,phoneNum,postCode;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstName.delegate = self;
    postCode.delegate = self;
    Address.delegate = self;
    phoneNum.delegate = self;
	// Do any additional setup after loading the view.
        //view isnt animated to enlarge it
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    
    
    id objJ=[[NSUserDefaults standardUserDefaults]objectForKey:@"firstName"];
    if (objJ == Nil) {

        [UIView beginAnimations:nil context:NULL];
        //[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [theView setFrame:CGRectMake(30, 70, 255, 300)];
        UIColor *background = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"viewBAckGround.png"]];
        self.theView.backgroundColor = background;
        theView.layer.cornerRadius = 10.0;
        theView.layer.masksToBounds = YES;
        [UIView commitAnimations];
        isAnimated=YES;
        theView.hidden=NO;
    }else {
        theView.hidden = YES;
        
    }

    [self.navigationController setToolbarHidden:NO  animated:YES];
    [[UIBarButtonItem appearance]setTintColor:[UIColor grayColor]];
    UIBarButtonItem *button0 = [[UIBarButtonItem alloc]initWithTitle:@"            " style:UIBarButtonItemStyleBordered target:self action:@selector(clickedButton1)];
    [button0 setBackgroundImage:[UIImage imageNamed:@"backButton_031.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(clickedButton1)];
    NSArray *itemsN = [NSArray arrayWithObjects:button0,button1, nil];
    [self setToolbarItems:itemsN animated:NO];
    self.navigationController.toolbar.clipsToBounds = YES;
}
-(IBAction)clickedButton1{
    UIAlertView *alertClose = [[UIAlertView alloc]initWithTitle:@"Property Link" message:@"Are you sure want to Logout?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    alertClose.tag = 2;
    [alertClose show];
    
  
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {

    [firstName resignFirstResponder];
    [Address resignFirstResponder];
    [phoneNum resignFirstResponder];
    [postCode resignFirstResponder];
    return NO;
}

-(IBAction)savedata {
  
    [firstName resignFirstResponder];
    [Address resignFirstResponder];
    [phoneNum resignFirstResponder];
    [postCode resignFirstResponder];
    
    if ([firstName.text isEqualToString:@""] || [phoneNum.text  isEqualToString: @""] || [Address.text isEqualToString: @""] || [postCode.text isEqualToString: @""]) {
        
        UIAlertView *alertEmpty = [[UIAlertView alloc]initWithTitle:@"Property Link" message:@"Please Enter Required Field" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alertEmpty show];
        
    }else {
    
    
    //create strings and integer to store text info
    NSString *firNameSave = [NSString stringWithFormat:@"%@",firstName.text];
    NSString *PhonNSave = [NSString stringWithFormat:@"%@",phoneNum.text];
    NSString *addreSave= [NSString stringWithFormat:@"%@",Address.text];
    NSString *postCSave = [NSString stringWithFormat:@"%@",postCode.text];
    
   
    //store data
    NSUserDefaults *defaultsData = [NSUserDefaults standardUserDefaults];
    [defaultsData setObject:firNameSave forKey:@"firstName"];
    [defaultsData setObject:PhonNSave forKey:@"phoneNum"];
    [defaultsData setObject:addreSave forKey:@"Address"];
    [defaultsData setObject:postCSave forKey:@"postCode"];
        NSLog(@"Data saved");
        [UIView beginAnimations:nil context:NULL];
        //[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        [theView setFrame:CGRectMake(320, 321, 0,0)];
        
        [UIView commitAnimations];
        isAnimated=NO;

    }
    
}
-(void) handleExit {
    //to set the animation to NO
    [UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [theView setFrame:CGRectMake(320, 321, 0,0)];
    
    [UIView commitAnimations];
    isAnimated=NO;
    
}

/*-(void) presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    NSUserDefaults *checkInital = [NSUserDefaults standardUserDefaults];
    BOOL hasShownSettings = [checkInital boolForKey:@"hasShownSettings"];
    
    
    if (!hasShownSettings) {
        myProperty *settingsVc = [[myProperty alloc]init];
        [self presentViewController:settingsVc animated:YES completion:^{
            
        
        [checkInital setBool:YES forKey:@"hasShownSettings"];
        }];
    }
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)maintenance {
    UIAlertView *myalert = [[UIAlertView alloc]initWithTitle:@"Property Link" message:@"Do you have picture of the faulty place or part that need service?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    myalert.tag =1;
    [myalert show];
    
}
-(void)alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
if (alertView.tag ==1) {
    if (buttonIndex ==0) {
        NSUserDefaults *getDefaults = [NSUserDefaults standardUserDefaults];
        NSString *firstNameGet = [getDefaults objectForKey:@"firstName"];
        NSString *addressGet = [getDefaults objectForKey:@"phoneNum"];
        NSString *phoneGet = [getDefaults objectForKey:@"Address"];
        NSString *postGet = [getDefaults objectForKey:@"postCode"];
        
        [[UINavigationBar appearance]setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        MFMailComposeViewController *mailTo = [[MFMailComposeViewController alloc]init];
        [mailTo setMailComposeDelegate:self];
        NSString *address = @"wakas@propertylinkestateagents.com";
        NSArray *addressArray = [[NSArray alloc]initWithObjects:address, nil];
        NSString *areaStr = [NSString stringWithFormat:@"%@\n%@ %@ %@ %@ %@ %@.%@\n%@%@",@"Dear Property Link",@"This Email is from Mr.",firstNameGet,@" and it is regarding to a maintenance concern which has arisen in my Property",phoneGet,@"at",postGet,@"The needed repair(s) is/are listed below:",@"Please Contact me my mobile Number, which is:",addressGet];
        // [mailTo addAttachmentData:receP.imgData mimeType:@"image/jpeg" fileName:areaStr];
        [mailTo setMessageBody:areaStr isHTML:NO];
        [mailTo setToRecipients:addressArray];
        [mailTo setSubject:[NSString stringWithFormat:@"%@,%@",phoneGet,postGet]];
        [mailTo setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        [[mailTo navigationBar]setTintColor:[UIColor purpleColor]];
        [mailTo setTitle:@" "];
        [self presentViewController:mailTo animated:YES completion:nil];

    
    }
       if (buttonIndex == 1) {
           
           UIImagePickerController *picker = [[UIImagePickerController alloc]init];
           picker.delegate = self;
           picker.allowsEditing = YES;
           picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
           [self presentViewController:picker animated:YES completion:Nil];
        
        }
    }
    if (alertView.tag ==2){
        if (buttonIndex ==0) {
          [self.navigationController popViewControllerAnimated:YES];
        }
    
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *imageGet = [info objectForKey:@"UIImagePickerControlleroriginalImage"];
    [self dismissViewControllerAnimated:YES completion:Nil];
    [self performSelector:@selector(emailToAtach:) withObject:imageGet afterDelay:1.0];    
}
-(void)emailToAtach:(UIImage *) image{
    

    NSUserDefaults *getDefaults = [NSUserDefaults standardUserDefaults];
    NSString *firstNameGet = [getDefaults objectForKey:@"firstName"];
    NSString *addressGet = [getDefaults objectForKey:@"phoneNum"];
    NSString *phoneGet = [getDefaults objectForKey:@"Address"];
    NSString *postGet = [getDefaults objectForKey:@"postCode"];
    
    [[UINavigationBar appearance]setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    MFMailComposeViewController *mailTo = [[MFMailComposeViewController alloc]init];
    [mailTo setMailComposeDelegate:self];
    NSString *address = @"wakas@propertylinkestateagents.com";
    NSArray *addressArray = [[NSArray alloc]initWithObjects:address, nil];
    NSString *areaStr = [NSString stringWithFormat:@"%@\n%@ %@ %@ %@ %@ %@.%@\n%@%@",@"Dear Property Link",@"This Email is from Mr.",firstNameGet,@" and it is regarding to a maintenance concern which has arisen in my Property",phoneGet,@"at",postGet,@"The needed repair(s) is/are listed below:",@"Please Contact me my mobile Number, which is:",addressGet];
    NSData *imgData = UIImageJPEGRepresentation(image,1);
    NSString *fileNameacc = @"Test";
    fileNameacc = [fileNameacc stringByAppendingPathExtension:@"jpeg"];
    [mailTo setMessageBody:areaStr isHTML:NO];
    [mailTo addAttachmentData:imgData mimeType:@"image/jpeg" fileName:fileNameacc];
    [mailTo setToRecipients:addressArray];
    [mailTo setSubject:[NSString stringWithFormat:@"%@,%@",phoneGet,postGet]];
    [mailTo setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [[mailTo navigationBar]setTintColor:[UIColor purpleColor]];
    [mailTo setTitle:@" "];
    [self presentViewController:mailTo animated:YES completion:nil];
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"search_navbar.png"] forBarMetrics:UIBarMetricsDefault];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)billing {
    NSUserDefaults *getDefaults = [NSUserDefaults standardUserDefaults];
    NSString *firstNameGet = [getDefaults objectForKey:@"firstName"];
    NSString *addressGet = [getDefaults objectForKey:@"phoneNum"];
    NSString *phoneGet = [getDefaults objectForKey:@"Address"];
    NSString *postGet = [getDefaults objectForKey:@"postCode"];
    
    [[UINavigationBar appearance]setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    MFMailComposeViewController *mailTo = [[MFMailComposeViewController alloc]init];
    [mailTo setMailComposeDelegate:self];
    NSString *address = @"wakas@propertylinkestateagents.com";
    NSArray *addressArray = [[NSArray alloc]initWithObjects:address, nil];
    NSString *areaStr = [NSString stringWithFormat:@"%@\n%@ %@ %@ %@ %@ %@.%@\n%@%@",@"Dear Property Link",@"This Email is from Mr.",firstNameGet,@" and it is regarding to a Billing concern of my Property",phoneGet,@"at",postGet,@"I like to discuss with you\n",@"Please Contact me my mobile Number, which is:",addressGet];
    // [mailTo addAttachmentData:receP.imgData mimeType:@"image/jpeg" fileName:areaStr];
    [mailTo setMessageBody:areaStr isHTML:NO];
    [mailTo setToRecipients:addressArray];
    [mailTo setSubject:[NSString stringWithFormat:@"%@,%@",phoneGet,postGet]];
    [mailTo setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [[mailTo navigationBar]setTintColor:[UIColor purpleColor]];
    [mailTo setTitle:@" "];
    [self presentViewController:mailTo animated:YES completion:nil];

}
-(IBAction)landLords {
    NSUserDefaults *getDefaults = [NSUserDefaults standardUserDefaults];
    NSString *firstNameGet = [getDefaults objectForKey:@"firstName"];
    NSString *addressGet = [getDefaults objectForKey:@"phoneNum"];
    NSString *phoneGet = [getDefaults objectForKey:@"Address"];
    NSString *postGet = [getDefaults objectForKey:@"postCode"];
    
    [[UINavigationBar appearance]setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    MFMailComposeViewController *mailTo = [[MFMailComposeViewController alloc]init];
    [mailTo setMailComposeDelegate:self];
    NSString *address = @"wakas@propertylinkestateagents.com";
    NSArray *addressArray = [[NSArray alloc]initWithObjects:address, nil];
    NSString *areaStr = [NSString stringWithFormat:@"%@\n%@ %@ %@ %@ %@ %@.%@\n%@%@",@"Dear Property Link",@"This Email is from Mr.",firstNameGet,@" and it is regarding to a about my Property",phoneGet,@"at",postGet,@"I like to discuss about my property with you.",@"Please Contact me my mobile Number, which is:",addressGet];
    // [mailTo addAttachmentData:receP.imgData mimeType:@"image/jpeg" fileName:areaStr];
    [mailTo setMessageBody:areaStr isHTML:NO];
    [mailTo setToRecipients:addressArray];
    [mailTo setSubject:[NSString stringWithFormat:@"%@,%@",phoneGet,postGet]];
    [mailTo setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [[mailTo navigationBar]setTintColor:[UIColor purpleColor]];
    [mailTo setTitle:@" "];
    [self presentViewController:mailTo animated:YES completion:nil];

}
-(IBAction)changeDetails {
    [UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.3];
    [theView setFrame:CGRectMake(30, 70, 255, 300)];
    UIColor *background = [[UIColor alloc]initWithPatternImage:[UIImage imageNamed:@"viewBAckGround.png"]];
    self.theView.backgroundColor = background;
    theView.layer.cornerRadius = 10.0;
    theView.layer.masksToBounds = YES;
    [UIView commitAnimations];
    isAnimated=YES;
    theView.hidden=NO;

    
    //callling the handle exit to close the button
    //adding Cancellation button to subView
    UIImage *buttonImage = [UIImage imageNamed:@"cl2.png"];
    UIButton *canx = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [canx addTarget:self action:@selector(handleExit) forControlEvents:UIControlEventTouchUpInside];
    [canx setFrame:CGRectMake(228, 2, 25 , 25)];
    [canx setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //[canx setTitle:@"X" forState:UIControlStateNormal];
    [theView addSubview:canx];
}




@end
