//
//  Favorites.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/12/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Favorites : NSManagedObject

@property (nonatomic, retain) NSString * houseNumber;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * houseArea;
@property (nonatomic, retain) NSString * housebeds;
@property (nonatomic, retain) NSString * housePrice;
@property (nonatomic, retain) NSString * houseName;
@property (nonatomic, retain) NSData   *imgD;

@end
