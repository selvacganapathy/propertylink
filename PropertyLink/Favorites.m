//
//  Favorites.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/12/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import "Favorites.h"


@implementation Favorites

@dynamic houseNumber;
@dynamic postCode;
@dynamic houseArea;
@dynamic housebeds;
@dynamic houseName;
@dynamic housePrice;
@dynamic imgD;

@end
