//
//  PropertyDetail.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import "PropertyDetail.h"
#import "propertyTable.h"
#import "propertyObject.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "Favorites.h"
#import <MapKit/MapKit.h>
#import "Annotation.h"

#define  hodgeHill @"01217831515"
#define  castle    @"01217831515"
#define  solihull  @"01217420233"
#define  sheldon   @"01217429977"
#define  bordese   @"01217831414"

@interface PropertyDetail ()
//MAPKIT
@property (weak, nonatomic) IBOutlet UITextField *zipOutlet;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet MKMapView *mapDist;
@property (strong, nonatomic) CLLocation *selectedLocation;
@property (strong, nonatomic) NSMutableDictionary *placeDictionary;


@end

@implementation PropertyDetail

@synthesize AddressLine,houseNumber,Postcode,Value,receP,dragImg,description1,Viewing;
@synthesize county;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize scrollInDetails,theView,locationmanager,distancL,imgScroll,pageContimages;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.hidesBackButton=YES;
    [description1 sizeToFit];
    Value.text = receP.price;
    Viewing.text = receP.latiDe;
    description1.frame = CGRectMake(10, 250, 300, 150);
    [description1 setScrollEnabled:YES];
    [description1 setUserInteractionEnabled:YES];
    description1.text = receP.hDesc;
    county.text = receP.textInputGlobalString;
    NSString *ChnageString = [receP.hCode uppercaseString];
    houseNumber.text = [NSString stringWithFormat:@"%@, %@",receP.hName,[ChnageString substringToIndex:3]];
    
    [super viewDidLoad];
    /*-----------------------------*/
    self.zipOutlet.delegate =self;
    self.mapView.delegate = self;
    //4
    self.placeDictionary = [[NSMutableDictionary alloc] init];
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 52.408;
    zoomLocation.longitude= 1.5106;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 1609.344,1609.344);
    [self.mapView setRegion:viewRegion animated:NO];
    _mapView.userInteractionEnabled =NO;
    self.mapView.layer.borderColor =[[UIColor purpleColor]CGColor];
    self.mapView.layer.borderWidth = 2.0;
    /*-----------------------------*/;
    
    
    locationmanager = [[CLLocationManager alloc]init];
    locationmanager.delegate = self;
    locationmanager.distanceFilter = 100.f;
    [locationmanager startUpdatingLocation];
    
    double lat =[receP.latiDe floatValue];
    double lng =[receP.longDe floatValue];
    CLLocation *loaction1 = [[CLLocation alloc]initWithLatitude:locationmanager.location.coordinate.latitude longitude:locationmanager.location.coordinate.longitude];
    CLLocation *location2 = [[CLLocation alloc]initWithLatitude:lat  longitude:lng];
    
    CLLocationDistance distance12 = [location2 distanceFromLocation:loaction1];
    NSLog(@"Distance=%f Miles",distance12);
    NSLog(@"property latitude:%f",lat);
    NSLog(@"Property longitude:%f",lng);
    NSLog(@"user Lction latitude:%f",locationmanager.location.coordinate.latitude);
    NSLog(@"user location longitude:%f",locationmanager.location.coordinate.longitude);
    if (lat !=0 &&lng !=0) {
    distancL.text = [NSString stringWithFormat:@"Distance:  %.1f miles",(distance12/1000)*0.62];
    }else {
    distancL.text = @"Calculating distance...";
    }
    
    [self.scrollInDetails setScrollEnabled:YES];
    [self.scrollInDetails setContentSize:CGSizeMake(320,850)];
    //self.automaticallyAdjustsScrollViewInsets = NO;
    
  
    
    AppDelegate *appDeleg = [[UIApplication sharedApplication]delegate];
    _managedObjectContext = [appDeleg managedObjectContext];
    
    //dragImg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:receipe.path]]];
    
 //ADD SCROLLVIEW TO THE VIEW
    int pageCount = 4;
    
    //UIScrollView *imgScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 30, 300, 199)];
    imgScroll.backgroundColor = [UIColor clearColor];
    imgScroll.pagingEnabled  = YES;
    imgScroll.contentSize = CGSizeMake(pageCount *imgScroll.bounds.size.width, imgScroll.bounds.size.height);
   
   
    //SET UP VIEW SIZE

    CGRect viewSize = imgScroll.bounds;
        
UIImageView *imgSc = [[UIImageView alloc]initWithFrame:viewSize];
    
    __weak UIImageView *weakCell = imgSc;
    [imgSc setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:receP.imgUR0]]
     
                 placeholderImage:[UIImage imageNamed:@"Placeholder.png"]
                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                              
                              weakCell.image = image;
                              
                              //only required if no placeholder is set to force the imageview on the cell to be laid out to house the new image.
                              //if(weakCell.imageView.frame.size.height==0 || weakCell.imageView.frame.size.width==0 ){
                              [weakCell setNeedsLayout];
                              //}
                          }
                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                          }]; //[NSString stringWithFormat:@"receP.imgUR%d",i+1]
    UIColor *borderColor = [UIColor purpleColor];
    [imgSc.layer setBorderColor:borderColor.CGColor];
    [imgSc.layer setBorderWidth:1.5f];
[imgScroll addSubview:imgSc];
    
        viewSize =CGRectOffset(viewSize, imgScroll.bounds.size.width, 0);
    
UIImageView *imgSc1 = [[UIImageView alloc]initWithFrame:viewSize];
    __weak UIImageView *weakCell1 = imgSc1;
    [imgSc1 setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:receP.imgUR1]]
     
                 placeholderImage:[UIImage imageNamed:@"Placeholder.png"]
                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                              
                              weakCell1.image = image;
                              
                              //only required if no placeholder is set to force the imageview on the cell to be laid out to house the new image.
                              //if(weakCell.imageView.frame.size.height==0 || weakCell.imageView.frame.size.width==0 ){
                              [weakCell1 setNeedsLayout];
                              //}
                          }
                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                            
                          }];
   
    [imgSc1.layer setBorderColor:borderColor.CGColor];
    [imgSc1.layer setBorderWidth:1.5f];
[imgScroll addSubview:imgSc1];
    
     viewSize =CGRectOffset(viewSize, imgScroll.bounds.size.width, 0);
    
UIImageView *imgSc2 = [[UIImageView alloc]initWithFrame:viewSize];
    __weak UIImageView *weakCell2 = imgSc2;
    [imgSc2 setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:receP.imgUR2]]
     
                 placeholderImage:[UIImage imageNamed:@"Placeholder.png"]
                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                              
                              weakCell2.image = image;
                              
                              //only required if no placeholder is set to force the imageview on the cell to be laid out to house the new image.
                              //if(weakCell.imageView.frame.size.height==0 || weakCell.imageView.frame.size.width==0 ){
                              [weakCell2 setNeedsLayout];
                              //}
                          }
                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                             
                          }];
    
    [imgSc2.layer setBorderColor:borderColor.CGColor];
    [imgSc2.layer setBorderWidth:1.5f];
[imgScroll addSubview:imgSc2];
   
    viewSize =CGRectOffset(viewSize, imgScroll.bounds.size.width, 0);
    
UIImageView *imgSc3 = [[UIImageView alloc]initWithFrame:viewSize];
    __weak UIImageView *weakCell3 = imgSc3;
    [imgSc3 setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:receP.imgUR3]]
     
                  placeholderImage:[UIImage imageNamed:@"Placeholder.png"]
                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                               
                               weakCell3.image = image;
                               
                               //only required if no placeholder is set to force the imageview on the cell to be laid out to house the new image.
                               //if(weakCell.imageView.frame.size.height==0 || weakCell.imageView.frame.size.width==0 ){
                               [weakCell3 setNeedsLayout];
                               //}
                           }
                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                              
                           }];
   
    [imgSc3.layer setBorderColor:borderColor.CGColor];
    [imgSc3.layer setBorderWidth:1.5f];
[imgScroll addSubview:imgSc3];
[self.scrollInDetails addSubview:imgScroll];

   /* __weak UIImageView *weakCell = imgSc;
        [imgSc setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:receP.imgUR0]]

                   placeholderImage:[UIImage imageNamed:@"Placeholder.png"]
                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
                              
                                weakCell.image = image;
                                
                                //only required if no placeholder is set to force the imageview on the cell to be laid out to house the new image.
                                //if(weakCell.imageView.frame.size.height==0 || weakCell.imageView.frame.size.width==0 ){
                                [weakCell setNeedsLayout];
                                //}
                            }
                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Loading...."
                                                                             message:nil//                           [NSString stringWithFormat:@"%@",error]
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                [av show];
                            }];*/
    
    dragImg.image = imgSc.image;
    [dragImg.layer setBorderColor:borderColor.CGColor];
    [dragImg.layer setBorderWidth:1.5f];
    [self.navigationController setToolbarHidden:NO  animated:YES];
    [[UIBarButtonItem appearance]setTintColor:[UIColor grayColor]];
    UIBarButtonItem *button0 = [[UIBarButtonItem alloc]initWithTitle:@"            " style:UIBarButtonItemStyleBordered target:self action:@selector(clickedButton1)];
    [button0 setBackgroundImage:[UIImage imageNamed:@"backButton_031.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(clickedButton1)];
    UIBarButtonItem *button2 = [[UIBarButtonItem alloc]initWithTitle:@"My Place" style:UIBarButtonItemStyleBordered target:self action:@selector(clickedButton2)];
    NSArray *itemsN = [NSArray arrayWithObjects:button0,button1,button2, nil];
    [self setToolbarItems:itemsN animated:NO];
    self.navigationController.toolbar.clipsToBounds = YES;
    [self updatePlaceDictionary];
    [self updateMaps];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float width = imgScroll.frame.size.width;
    float xPos = imgScroll.contentOffset.x+10;
    pageContimages.currentPage = (int) xPos/width;
}

-(IBAction)clickedButton1{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)clickedButton2 {
        
        if(isAnimated) {
        }
        
        else{
            //view isnt animated to enlarge it
            [UIView beginAnimations:nil context:NULL];
            //[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDuration:0.3];
            [theView setFrame:CGRectMake(10, 230, 300, 230)];
            [UIView commitAnimations];
            isAnimated=YES;
            theView.hidden=NO;
            
            //adding Cancellation button to subView
            UIImage *buttonImage = [UIImage imageNamed:@"cl2.png"];
            UIButton *canx = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            
         /*   MKMapView *mapView1 = [[MKMapView alloc]initWithFrame:CGRectMake(10, 10, 280,210)];
            mapView1.frame = CGRectMake(10, 10, 280, 210);
            mapView1.showsUserLocation =YES;
           MKCoordinateRegion region;
            region.center = mapView1.userLocation.coordinate;
            region.span = MKCoordinateSpanMake(0.1, 0.1);
                        region = [mapView1 regionThatFits:region];
            [mapView1 setRegion:region animated:YES];
            mapView1.layer.cornerRadius= 10;
            mapView1.layer.masksToBounds =YES;
            theView.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0.9];
            theView.opaque = NO;
            [self.theView addSubview:mapView1];*/
            
            MKMapView *mapView1 = [[MKMapView alloc]initWithFrame:CGRectMake(10, 10, 280,210)];
            mapView1.frame = CGRectMake(10, 10, 280, 210);
            mapView1.showsUserLocation =YES;
            mapView1.delegate = self;
            [mapView1 setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
            mapView1.mapType = MKMapTypeStandard;
            mapView1.layer.cornerRadius= 10;
            mapView1.layer.masksToBounds =YES;
            theView.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0.9];
            theView.opaque = NO;
            [self.theView addSubview:mapView1];
            


            //callling the handle exit to close the button
            [canx addTarget:self action:@selector(handleExit) forControlEvents:UIControlEventTouchUpInside];
            [canx setFrame:CGRectMake(258, 8, 34 , 34)];
            [canx setBackgroundImage:buttonImage forState:UIControlStateNormal];
            //[canx setTitle:@"X" forState:UIControlStateNormal];
            [theView addSubview:canx];
        }
    }
    
-(void) handleExit {
        //to set the animation to NO
        [UIView beginAnimations:nil context:NULL];
        //[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        [theView setFrame:CGRectMake(320, 321, 0,0)];
      
        [UIView commitAnimations];
        isAnimated=NO;
        
    }

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"error message - %@",[error description]);
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bookMark{
    
   UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Add to Favorities?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"YES" otherButtonTitles:nil, nil];
    [actionSheet setTag:1];
    [actionSheet showInView:self.view];
}
-(IBAction)callNumber{
    
    UIActionSheet *actionSheet1 = [[UIActionSheet alloc]initWithTitle:@"Contact Us" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Hodge Hill",@"Castle Bromwich",@"Solihull",@"Sheldon",@"Boreseley Green", nil];
    [actionSheet1 setTag:2];
    [actionSheet1 showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
   
    if (actionSheet.tag ==1) {
                if (buttonIndex ==0) {
            Favorites *favContent = [NSEntityDescription insertNewObjectForEntityForName:@"Favorites" inManagedObjectContext:_managedObjectContext];
            [favContent setPostCode:receP.hCode];
            [favContent setHouseArea:receP.hArea];
            [favContent setHousebeds:receP.hBeds];
            [favContent setHouseName:receP.hName];
            [favContent setHouseNumber:receP.hNumber];
            [favContent setHousePrice:receP.price];
            NSData *dataI = UIImagePNGRepresentation(self.dragImg.image);
            [favContent setImgD:dataI];
                    
                   
                    
                    //SAVE THAT TO THE DATABASE
                    NSError *error =Nil;
                    if(![_managedObjectContext save:&error]) {/*HANDLE ERROR*/ NSLog(@"Unresolved Error %@ %@",  error,[error userInfo]);}
                    
        }
        
    }
else if (actionSheet.tag == 2){
        if (buttonIndex ==0) {
            NSURL *phoneCallButton = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",hodgeHill]];
            [[UIApplication sharedApplication]openURL:phoneCallButton];
        }else if (buttonIndex ==1){
            NSURL *phoneCallButton1 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",castle]];
            [[UIApplication sharedApplication]openURL:phoneCallButton1];
            
        }else if (buttonIndex ==2){
            NSURL *phoneCallButton2 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",solihull]];
            [[UIApplication sharedApplication]openURL:phoneCallButton2];
            
        }else if (buttonIndex ==3){
            NSURL *phoneCallButton3 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",sheldon]];
            [[UIApplication sharedApplication]openURL:phoneCallButton3];
            
        }else if (buttonIndex ==4){
            NSURL *phoneCallButton4 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",bordese]];
            [[UIApplication sharedApplication]openURL:phoneCallButton4];
            
        }
    }
}

-(IBAction)emailTo{
    [[UINavigationBar appearance]setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    MFMailComposeViewController *mailTo = [[MFMailComposeViewController alloc]init];
    [mailTo setMailComposeDelegate:self];
    NSString *address = @"wakas@propertylinkestateagents.com";
    NSArray *addressArray = [[NSArray alloc]initWithObjects:address, nil];
    NSString *ChnageString = [receP.hCode uppercaseString];
    [NSString stringWithFormat:@"%@, %@",receP.hName,[ChnageString substringToIndex:3]];
    NSString *areaStr = [NSString stringWithFormat:@"%@ \n, %@ %@\n\n%@",@"I thought you might want to take a look at this property to rent on Property link:",receP.hName,[ChnageString substringToIndex:3],receP.imgUR0];
    [mailTo addAttachmentData:receP.imgData mimeType:@"image/jpeg" fileName:areaStr];
    [mailTo setMessageBody:areaStr isHTML:NO];
    [mailTo setToRecipients:addressArray];
    [mailTo setSubject:@"PropertyLink"];
    [mailTo setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [[mailTo navigationBar]setTintColor:[UIColor purpleColor]];
    [mailTo setTitle:@" "];
    [self presentViewController:mailTo animated:YES completion:nil];
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"search_navbar.png"] forBarMetrics:UIBarMetricsDefault];
    [self dismissViewControllerAnimated:YES completion:nil];

}
-(IBAction)requestViewTo:(id)sender{
    
   
    [[UINavigationBar appearance]setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    MFMailComposeViewController *mailTo = [[MFMailComposeViewController alloc]init];
    [mailTo setMailComposeDelegate:self];
    NSString *address = @"wakas@propertylinkestateagents.com";
    NSArray *addressArray = [[NSArray alloc]initWithObjects:address, nil];
    NSString *ChnageString = [receP.hCode uppercaseString];
    [NSString stringWithFormat:@"%@, %@",receP.hName,[ChnageString substringToIndex:3]];
     NSString *areaStr = [NSString stringWithFormat:@"%@ \n, %@ %@\n\n%@",@"Please Contact me to Arrange a viewing of this Property:",receP.hName,[ChnageString substringToIndex:3],receP.imgUR0];
    [mailTo addAttachmentData:receP.imgData mimeType:@"image/jpeg" fileName:areaStr];
    [mailTo setMessageBody:areaStr isHTML:NO];
    [mailTo setToRecipients:addressArray];
    [mailTo setSubject:@"PropertyLink"];
    [mailTo setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [[mailTo navigationBar]setTintColor:[UIColor purpleColor]];
    [mailTo setTitle:@" "];
    [self presentViewController:mailTo animated:YES completion:nil];
}
- (void)updatePlaceDictionary {
    //5
    //[self.placeDictionary setValue:self.addressOutlet.text forKey:@"Street"];
    //[self.placeDictionary setValue:self.cityOutlet.text forKey:@"City"];
    //[self.placeDictionary setValue:self.stateOutlet.text forKey:@"State"];
    [self.placeDictionary setValue:receP.hCode /*self.zipOutlet.text*/ forKey:@"ZIP"];
}

- (void)updateMaps {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressDictionary:self.placeDictionary completionHandler:^(NSArray *placemarks, NSError *error) {
        if([placemarks count]) {
            
            
            NSMutableArray *locatonArray = [[NSMutableArray alloc]init];
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            CLLocation *location = placemark.location;
            
            CLLocationCoordinate2D coordinate = location.coordinate;
            [self.mapView setCenterCoordinate:coordinate animated:NO];
            
           /* MKPointAnnotation *pointAn = [[MKPointAnnotation alloc]init];
            
            [pointAn setCoordinate:coordinate];
            [pointAn setTitle:houseNumber.text];
            [self.mapView addAnnotation:pointAn];*/
            
            double lat =[receP.latiDe floatValue];
            double lng =[receP.longDe floatValue];
            
           
            CLLocationCoordinate2D locationAnnot;
            Annotation *myAnn3;
            
            myAnn3 = [[Annotation alloc]init];
            locationAnnot.latitude = lat;
            locationAnnot.longitude=lng;
            myAnn3.coordinate = locationAnnot;
            myAnn3.title =@"Property Location";
            NSString *ChnageString = [receP.hCode uppercaseString];
            myAnn3.subtitle=[NSString stringWithFormat:@"%@, %@",receP.hName,[ChnageString substringToIndex:3]];
            [locatonArray addObject:myAnn3];
            
            myAnn3 = [[Annotation alloc]init];
            locationAnnot.latitude = locationmanager.location.coordinate.latitude;
            locationAnnot.longitude=locationmanager.location.coordinate.longitude;
            myAnn3.coordinate = locationAnnot;
            myAnn3.title =@"Current Location";
            myAnn3.subtitle=@"";
            [locatonArray addObject:myAnn3];
            [self.mapView addAnnotations:locatonArray];
        
         /*   CLLocationCoordinate2D currentLoc;
            currentLoc.latitude = locationmanager.location.coordinate.latitude;
            currentLoc.longitude= locationmanager.location.coordinate.longitude;
            
            Annotation *myAnn2 = [Annotation alloc];
            myAnn2.coordinate = currentLoc;
            myAnn2.title = @"Current Location";
            myAnn2.subtitle=receP.hCode;
          //  [self.mapView addAnnotation:myAnn2];*/
            
            

            
        } else {
            NSLog(@"error");
        }
    }];
}
- (void)delayedReverseGeocodeLocation {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self reverseGeocodeLocation];
    
}
- (void)reverseGeocodeLocation {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:self.selectedLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if(placemarks.count){
            NSDictionary *dictionary = [[placemarks objectAtIndex:0] addressDictionary];
            // [self.addressOutlet setText:[dictionary valueForKey:@"Street"]];
            //  [self.cityOutlet setText:[dictionary valueForKey:@"City"]];
            // [self.stateOutlet setText:[dictionary valueForKey:@"State"]];
            [self.zipOutlet setText:[dictionary valueForKey:@"ZIP"]];
        }
    }];
}
#pragma mark - MapView Delegate Methods
-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    self.selectedLocation =
    [[CLLocation alloc] initWithLatitude:mapView.centerCoordinate.latitude
                               longitude:mapView.centerCoordinate.longitude];
    [self performSelector:@selector(delayedReverseGeocodeLocation)
               withObject:nil
               afterDelay:0.3];
}
#pragma mark - TextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*- (IBAction)submitTapped:(id)sender {
    [self updatePlaceDictionary];
    [self updateMaps];
}*/
-(IBAction)zoomIn:(id)sender{
    
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta /=2;
    region.span.longitudeDelta /=2;
    [self.mapView setRegion:region animated:YES];
}
-(IBAction)zoomOut:(id)sender{
    
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta =MIN(region.span.latitudeDelta *2.0, 180.0);
    region.span.longitudeDelta = MIN(region.span.longitudeDelta *2.0, 180.0);
    [self.mapView setRegion:region animated:YES];
}

@end
