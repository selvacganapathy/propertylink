//
//  favoritestableView.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/12/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import "favoritestableView.h"
#import "AppDelegate.h"
#import "Favorites.h"
#import "AFNetworking.h"

@interface favoritestableView ()

@end

@implementation favoritestableView
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize favoritesSearchArray,dragImg1;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton=YES;
    
    self.tableView.tableFooterView = [[UIView alloc]init];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    _managedObjectContext =[appDelegate managedObjectContext];
    
    //grab the data nsfetech request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *bikes = [NSEntityDescription entityForName:@"Favorites" inManagedObjectContext:_managedObjectContext];
    [request setEntity:bikes];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"postCode" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[_managedObjectContext executeFetchRequest:request error:&error]mutableCopy];
    if(mutableFetchResults == nil) {
        
        //handle error
    }
    [self setFavoritesSearchArray:mutableFetchResults];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.navigationController setToolbarHidden:NO  animated:YES];
    [[UIBarButtonItem appearance]setTintColor:[UIColor grayColor]];
    UIBarButtonItem *button0 = [[UIBarButtonItem alloc]initWithTitle:@"            " style:UIBarButtonItemStyleBordered target:self action:@selector(clickedButton1)];
    [button0 setBackgroundImage:[UIImage imageNamed:@"backButton_031.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(clickedButton1)];
    NSArray *itemsN = [NSArray arrayWithObjects:button0,button1, nil];
    [self setToolbarItems:itemsN animated:NO];
    self.navigationController.toolbar.clipsToBounds = YES;
}

-(IBAction)clickedButton1{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return favoritesSearchArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FavCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Favorites *Fav = (Favorites *) [favoritesSearchArray objectAtIndex:indexPath.row];
    
    // Configure the cell...
    
    //cell.textLabel.text =[Fav postCode];
    UILabel *houseName = (UILabel *) [cell viewWithTag:20];
    houseName.text = Fav.houseName;
   
    UILabel *pricelabel = (UILabel *) [cell viewWithTag:30];
    pricelabel.text = [NSString stringWithFormat:@"%@ %@",@"£",Fav.housePrice];

    
    UILabel *bedslabel = (UILabel *) [cell viewWithTag:50];
    bedslabel.text = Fav.housebeds;
    
    UILabel *areaLabel = (UILabel *) [cell viewWithTag:60];
    NSString *ChnageString = [Fav.postCode uppercaseString];
    areaLabel.text = [NSString stringWithFormat:@"%@, %@",Fav.houseArea,[ChnageString substringToIndex:3]];
    
    UIImage *imgBytes = [UIImage imageWithData:[Fav valueForKey:@"imgD"]];
    cell.imageView.image = imgBytes;
    [tableView setSeparatorColor:[UIColor clearColor]];
    tableView.layer.cornerRadius =20;
    cell.layer.borderColor = [[UIColor purpleColor]CGColor];
    cell.layer.borderWidth = 1.5f;
    [cell.layer setCornerRadius:5];
    cell.indentationWidth =10;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObject *eventToDelete = [favoritesSearchArray objectAtIndex:indexPath.row];
        [_managedObjectContext deleteObject:eventToDelete];
        
        
        [favoritesSearchArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, Nil] withRowAnimation:YES];
        
        NSError *error = Nil;
        if (![_managedObjectContext save:&error]) {
            
        }
        
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
 
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
