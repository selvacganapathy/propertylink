
//  propertyTable.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import "propertyTable.h"
#import "propertyObject.h"
#import "PropertyDetail.h"
#import "AFNetworking.h"
#import <QuartzCore/QuartzCore.h>
#import "searchView.h"

@interface propertyTable ()

@end

@implementation propertyTable

@synthesize jSonArray,propertyArray,propertyListTableView;
@synthesize textfieldarea,introString,saleType2;
@synthesize toolbar;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self retreiveData];
    
    self.navigationItem.hidesBackButton=YES;
    self.tableView.tableFooterView = [[UIView alloc]init];
    [propertyListTableView.layer setBorderWidth: 1.0];
    [propertyListTableView.layer setCornerRadius:8.0f];
    [propertyListTableView.layer setMasksToBounds:YES];
    [propertyListTableView.layer setBorderColor:[[UIColor blackColor] CGColor]];
    

    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [self.navigationController setToolbarHidden:NO  animated:YES];
    UIImage *toolbarImage = [[UIImage imageNamed:@"search_BottomBar1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UIToolbar appearance]setBackgroundImage:toolbarImage forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance]setTintColor:[UIColor blackColor]];
    UIBarButtonItem *button0 = [[UIBarButtonItem alloc]initWithTitle:@"            " style:UIBarButtonItemStyleBordered target:self action:@selector(clickedButton1)];
    [button0 setBackgroundImage:[UIImage imageNamed:@"backButton_031.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(clickedButton1)];
    NSArray *itemsN = [NSArray arrayWithObjects:button0,button1, nil];
    [self setToolbarItems:itemsN animated:NO];
    self.navigationController.toolbar.clipsToBounds = YES;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel * sectionHeader = [[UILabel alloc] initWithFrame:CGRectZero];
    sectionHeader.backgroundColor = [UIColor whiteColor];
    sectionHeader.font = [UIFont fontWithName:@"Helvetica" size:17];
    sectionHeader.textColor = [UIColor purpleColor];

    if(section == 0)
    {
        sectionHeader.text = [NSString stringWithFormat:@" %@,  %lu %@",introString,(unsigned long)propertyArray.count,@"results found"];
    }
        return sectionHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0)
    {
       return 25.0;
        
    }
    else
    {
        return 10.0;
    }
}
-(IBAction)clickedButton1{
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.propertyArray.count ==0) {
        UIAlertView *alerts = [[UIAlertView alloc]initWithTitle:@"Property Alert!" message:@"No House for Sale in  selected Area" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:Nil, nil];
        [alerts show];

    }
       return self.propertyArray.count;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex ==0) {
        UIViewController *anotheVCC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchView"];
        [self.navigationController pushViewController:anotheVCC animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == Nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    propertyObject *propertyList = [propertyArray objectAtIndex:indexPath.row];
    
    //PASSING THE DATA TO UILABELS
    UILabel *houseNumber = (UILabel *) [cell viewWithTag:20];
    houseNumber.text = propertyList.hName;
    
if ([saleType2  isEqual:@"16"]) {
    UILabel *pricelabel = (UILabel *) [cell viewWithTag:30];
    //pricelabel.text = propertyList.price;
    pricelabel.text = [NSString stringWithFormat:@"%@ %@ %@",@"£",propertyList.price,@"PM"];
}else{
    UILabel *pricelabel = (UILabel *) [cell viewWithTag:30];
    pricelabel.text = [NSString stringWithFormat:@"%@ %@",@"£",propertyList.price];}
    
    UILabel *postCodelabel = (UILabel *) [cell viewWithTag:40];
    NSString *changeString = [propertyList.hCode uppercaseString];
    postCodelabel.text = [NSString stringWithFormat:@"%@",[changeString substringToIndex:3]];
    
    UILabel *bedslabel = (UILabel *) [cell viewWithTag:50];
    bedslabel.text = propertyList.hBeds;
    
    UILabel *areaLabel = (UILabel *) [cell viewWithTag:60];
    areaLabel.text = propertyList.hArea;
 
    
    __weak UITableViewCell *weakcell = cell;
    [cell.imageView setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:propertyList.imgUR0]] placeholderImage:[UIImage imageNamed:@"Placeholder1.png"]
                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       //MAKE THE IMAGE LOAD ON THE AFNETWORKING
                                       // weakcell.imageView.image = image;
                                       // cell.imageView.image = [image resizeImage: newSize:CGRectMake(0, 0, 50, 50)];
                                       // weakcell.imageView.contentMode = UIViewContentModeScaleAspectFit;
                                       // [weakcell setNeedsLayout];
                                       // UIImageView *myView =  [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
                                       // [myView setImage:image];
                                       //[self.propertyListTableView reloadData];
                                       // weakcell.imageView.bounds = CGRectMake(0,0,75,75);
                                       //weakcell.imageView.frame = CGRectMake(0,0,75,75);
                                       //weakcell.imageView.contentMode = UIViewContentModeScaleAspectFit;
                                        
                                        UIImageView * iv = [[UIImageView alloc] initWithImage:image];
                                        iv.frame = (CGRect){{3,7},{85,55}};
                                        iv.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin| UIViewAutoresizingFlexibleRightMargin;
                                        iv.contentMode = UIViewContentModeScaleToFill;
                                        
                                        //[weakcell.contentView addSubview:iv];
                                        [weakcell addSubview:iv];
                                        
    }
                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                        UIAlertView *AV = [[UIAlertView alloc] initWithTitle:@"please Wait..." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                        
                                        [AV show];
                                    
    }];
    
    // Configure the cell...
    //cell.imageView.clipsToBounds = YES;
    //[cell.imageView setFrame:CGRectMake(0, 0, 30, 30)];
    //[cell.imageView setContentMode:UIViewContentModeScaleToFill];
    //[cell.imageView setAutoresizingMask:UIViewAutoresizingNone];
    //cell.imageView.layasksToBounds =YES;
    //cell.imageView.layer.cornerRadius=10;
    /*[cell setBackgroundColor:[UIColor blackColor]];*/
    //UIView *roundView = [[UIView alloc]initWithFrame:cell.frame];
    //[roundView setBackgroundColor:[UIColor whiteColor]];
    //roundView.layer.cornerRadius = 10.0;
    //[[cell contentView]addSubview:roundView];
    /*self.propertyListTableView.layer.borderWidth =2;
    self.propertyListTableView.layer.borderColor=[[UIColor purpleColor]CGColor];
   // self.propertyListTableView.layer.cornerRadius=5;*/
    [propertyListTableView setSeparatorColor:[UIColor clearColor]];
    propertyListTableView.layer.cornerRadius =20;
    cell.layer.borderColor = [[UIColor purpleColor]CGColor];
    cell.layer.borderWidth = 1.5f;
    [cell.layer setCornerRadius:5];
    cell.indentationWidth =10;
    return cell;
}


-(void)retreiveData {
 
    NSLog(@"sale in tableview:%@",saleType2);
    NSLog(@"text in tableview:%@",introString);

    NSString *myStrig = [NSString stringWithFormat:@"http://www.sas-learner.net/prop_fetch_res.php?username=%@&password=%@&valuearea=%@&saletype=%@",@"web160-propert-1",@"9l09lt41iuks",[introString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],saleType2];
    
    NSURL *myurl =[NSURL URLWithString:myStrig];
    
    NSData *data = [NSData dataWithContentsOfURL:myurl];
    
    jSonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    propertyArray = [[NSMutableArray alloc]init];

    for (int i = 0; i<jSonArray.count; i++)
        {
            //CREATING NSTRING OBJECTS
            NSString *hNumber = [[jSonArray objectAtIndex:i] objectForKey:@"street_num"];
            NSString *hName  =  [[jSonArray objectAtIndex:i] objectForKey:@"street"];
            NSString *hArea   = [[jSonArray objectAtIndex:i] objectForKey:@"region"];
            NSString *hCode   = [[jSonArray objectAtIndex:i] objectForKey:@"postcode"];
            NSString *hBeds   = [[jSonArray objectAtIndex:i] objectForKey:@"beds"];
            NSString *price   = [[jSonArray objectAtIndex:i] objectForKey:@"price"];
            NSString *imgUR0  = [[jSonArray objectAtIndex:i] objectForKey:@"IFNULL(IMG0.IMG0,0)"];
            NSString *imgUR1 =  [[jSonArray objectAtIndex:i] objectForKey:@"IFNULL(IMG5.IMG5,0)"];
            NSString *imgUR2  = [[jSonArray objectAtIndex:i] objectForKey:@"IFNULL(IMG2.IMG2,0)"];
            NSString *imgUR3  = [[jSonArray objectAtIndex:i] objectForKey:@"IFNULL(IMG3.IMG3,0)"];
            NSString *hDesc   = [[jSonArray objectAtIndex:i] objectForKey:@"description"];
            NSString *latiDe  = [[jSonArray objectAtIndex:i] objectForKey:@"latitude"];
            NSString *longDe  = [[jSonArray objectAtIndex:i] objectForKey:@"longitude"];
            
            
            
            //ASSIGN TO PROPERTY LIST OBJECTS
            propertyObject *myProperty = [[propertyObject alloc] initWithhnumber:hNumber andhName:hName andhArea:hArea andhCode:hCode andhBeds:hBeds andprice:price andimgUR0:imgUR0 andimgUR1:imgUR1 andimgUR2:imgUR2 andimgURL3:imgUR3 andhDesc:hDesc andlatiDe:latiDe andlongDe:longDe];
                                          
            [propertyArray addObject:myProperty];
            NSLog(@"%@",propertyArray);
            
        }
    
        [self.propertyListTableView reloadData];
    }


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"DPConnect"]){
        NSIndexPath *indexPath = [self.propertyListTableView indexPathForSelectedRow];
        PropertyDetail *destCont=segue.destinationViewController;
        
        destCont.receP = [propertyArray objectAtIndex:indexPath.row];
        
        // [self.navigationController pushViewController:destCont animated:YES];
        
        //BIkes *currentBike = [makeArray objectAtIndex:indexPath.row];
        
        //destCont.condition = currentBike.conDition;
        //destCont.mileage = currentBike.mileage;
        // destCont.engineSize = currentBike.engineSize;
    }
}


@end
