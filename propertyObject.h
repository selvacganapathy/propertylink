//
//  propertyObject.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface propertyObject : NSObject

//DECLARATION OF PROPERTY OBJECTS
@property (nonatomic, strong) NSString *hNumber;
@property (nonatomic, strong) NSString *hName;
@property (nonatomic, strong) NSString *hArea;
@property (nonatomic, strong) NSString *hCode;
@property (nonatomic, strong) NSString *hBeds;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *imgUR0;
@property (nonatomic, strong) NSString *imgUR1;
@property (nonatomic, strong) NSString *imgUR2;
@property (nonatomic, strong) NSString *imgUR3;
@property (nonatomic, strong) NSString *hDesc;
@property (nonatomic, strong) NSString *latiDe;
@property (nonatomic, strong) NSString *longDe;

//DECLARATION FOR TEXTFIELD
@property (nonatomic,strong) NSString *textInputGlobalString;
@property (nonatomic,strong) NSString *saleType;



//DECLARING IMAGE DATA- TO FAST FOR AFNETWORING
@property (nonatomic, strong) NSData *imgData;
@property (nonatomic, strong) NSData *imgData1;
@property (nonatomic, strong) NSData *imgData2;
@property (nonatomic, strong) NSData *imgData3;

//LOAD IMAGE FASTER IN DETAIL VIEWCONTROLLER
-(void) loadData;
//DECLARING METHODS
-(id) initWithhnumber: (NSString *) hNo andhName: (NSString *) hNme andhArea: (NSString *) hAra andhCode: (NSString *) hCde andhBeds: (NSString *) hBds andprice:(NSString *) pRce andimgUR0: (NSString *) imageUR0 andimgUR1: (NSString *) imgur1 andimgUR2: (NSString *) imgur2 andimgURL3: (NSString *) imgur3 andhDesc: (NSString *) descrip andlatiDe: (NSString *) latitudE andlongDe: (NSString *) longitudE;
//LOAD TEXTINPUT TP GLOBAL
-(id) initWithtextInputGlobalString: (NSString *) textInGloStr;

-(id) initWithsaleType: (NSString*) saleTypeStr;


@end
