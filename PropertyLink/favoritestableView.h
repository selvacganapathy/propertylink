//
//  favoritestableView.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/12/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface favoritestableView : UITableViewController

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic,strong)NSMutableArray *favoritesSearchArray;
@property (nonatomic,retain) IBOutlet UIImageView *dragImg1;
@end
