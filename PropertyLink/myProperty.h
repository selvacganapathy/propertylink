//
//  myProperty.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 14/01/2014.
//  Copyright (c) 2014 SCube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>

@interface myProperty : UIViewController  <MFMailComposeViewControllerDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    BOOL isAnimated;
    
}
@property (weak, nonatomic) IBOutlet UIView *theView;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *Address;
@property (weak, nonatomic) IBOutlet UITextField *phoneNum;
@property (weak, nonatomic) IBOutlet UITextField *postCode;

-(IBAction)savedata;
-(IBAction)maintenance;
-(IBAction)billing;
-(IBAction)landLords;
-(IBAction)changeDetails;
@end
