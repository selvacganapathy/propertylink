//
//  Latestsearch.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 08/12/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Latestsearch : NSManagedObject

@property (nonatomic, retain) NSString * search;
@property (nonatomic, retain) NSString * time;
@end
