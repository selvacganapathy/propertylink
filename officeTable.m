//
//  officeTable.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 10/12/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import "officeTable.h"

#define  hodgeHill @"01217831515"
#define  castle    @"01217831515"
#define  solihull  @"01217420233"
#define  sheldon   @"01217429977"
#define  bordese   @"01217831414"
@interface officeTable ()

@end

@implementation officeTable {
    
    NSArray *tableArrayOffice;
    
}
@synthesize officeTableView;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc]init];
    //tableArrayOffice = [NSArray arrayWithObjects:@"Hodge Hill Office:-",@"Sheldon Office:-", nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.navigationController setToolbarHidden:NO  animated:YES];
    [[UIBarButtonItem appearance]setTintColor:[UIColor grayColor]];
    UIBarButtonItem *button0 = [[UIBarButtonItem alloc]initWithTitle:@"            " style:UIBarButtonItemStyleBordered target:self action:@selector(clickedButton1)];
    [button0 setBackgroundImage:[UIImage imageNamed:@"backButton_031.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(clickedButton1)];
    NSArray *itemsN = [NSArray arrayWithObjects:button0,button1, nil];
    [self setToolbarItems:itemsN animated:NO];
    self.navigationController.toolbar.clipsToBounds = YES;
}

-(IBAction)clickedButton1{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [officeTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell.textLabel.text=[tableArrayOffice objectAtIndex:indexPath.row];
    if (indexPath.row ==0) {
        UILabel *headerLine = (UILabel *) [cell viewWithTag:10];
        headerLine.text = @"Hodge Hill Office:-";
        UILabel *firstLine = (UILabel *) [cell viewWithTag:20];
        firstLine.text = @"1 ColeHill Road";
        UILabel *secLine = (UILabel *) [cell viewWithTag:30];
        secLine.text = @"Birmingham";
        UILabel *thrLine = (UILabel *) [cell viewWithTag:40];
        thrLine.text=@"B36 8DT";
        
        callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [callButton addTarget:self action:@selector(colorButton) forControlEvents:UIControlEventTouchUpInside];
        [callButton setTitle:@"Call" forState:UIControlStateNormal];
        [callButton setBackgroundColor:[UIColor purpleColor]];
        callButton.frame = CGRectMake(270,0, 50, 30);
        [cell addSubview:callButton];

    }else if (indexPath.row ==1){
        UILabel *headerLine = (UILabel *) [cell viewWithTag:10];
        headerLine.text = @"Sheldon Office:-";
        UILabel *firstLine = (UILabel *) [cell viewWithTag:20];
        firstLine.text = @"2198 Coventry Road Sheldon";
        UILabel *secLine = (UILabel *) [cell viewWithTag:30];
        secLine.text = @"Birmingham";
        UILabel *thrLine = (UILabel *) [cell viewWithTag:40];
        thrLine.text=@"B26 3JH";
        
        callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [callButton addTarget:self action:@selector(colorButton) forControlEvents:UIControlEventTouchUpInside];
        [callButton setTitle:@"Call" forState:UIControlStateNormal];
        [callButton setBackgroundColor:[UIColor purpleColor]];
        callButton.frame = CGRectMake(270,0, 50, 30);
        [cell addSubview:callButton];
    }
    
    // Configure the cell...
    cell.layer.borderColor = [[UIColor purpleColor]CGColor];
    cell.layer.borderWidth = 1.5f;
    [cell.layer setCornerRadius:5];

    return cell;
}
-(IBAction)colorButton{
    
    UIActionSheet *actionSheet1 = [[UIActionSheet alloc]initWithTitle:@"Contact Us" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Hodge Hill",@"Castle Bromwich",@"Solihull",@"Sheldon",@"Boreseley Green", nil];
    [actionSheet1 setTag:2];
    [actionSheet1 showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
if (actionSheet.tag == 2){
        if (buttonIndex ==0) {
            NSURL *phoneCallButton = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",hodgeHill]];
            [[UIApplication sharedApplication]openURL:phoneCallButton];
        }else if (buttonIndex ==1){
            NSURL *phoneCallButton1 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",castle]];
            [[UIApplication sharedApplication]openURL:phoneCallButton1];
            
        }else if (buttonIndex ==2){
            NSURL *phoneCallButton2 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",solihull]];
            [[UIApplication sharedApplication]openURL:phoneCallButton2];
            
        }else if (buttonIndex ==3){
            NSURL *phoneCallButton3 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",sheldon]];
            [[UIApplication sharedApplication]openURL:phoneCallButton3];
            
        }else if (buttonIndex ==4){
            NSURL *phoneCallButton4 = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"tel://%@",bordese]];
            [[UIApplication sharedApplication]openURL:phoneCallButton4];
            
        }
    }
}

@end
