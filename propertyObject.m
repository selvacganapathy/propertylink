//
//  propertyObject.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//
#import "propertyObject.h"
#import "searchView.h"
@implementation propertyObject
@synthesize hNumber;
@synthesize hName;
@synthesize hArea;
@synthesize hCode;
@synthesize hBeds;
@synthesize price;
@synthesize imgUR0;
@synthesize hDesc;
@synthesize imgData;
@synthesize textInputGlobalString;
@synthesize saleType;
@synthesize imgUR1;
@synthesize imgUR2;
@synthesize imgUR3;
@synthesize latiDe;
@synthesize longDe;


//DECLARING METHODS TO STRING
-(id)initWithhnumber:(NSString *)hNo andhName:(NSString *)hNme andhArea:(NSString *)hAra andhCode:(NSString *)hCde andhBeds:(NSString *)hBds andprice:(NSString *)pRce andimgUR0:(NSString *)imageUR0 andimgUR1:(NSString *)imgur1 andimgUR2:(NSString *)imgur2 andimgURL3:(NSString *)imgur3 andhDesc:(NSString *)descrip andlatiDe:(NSString *)latitudE andlongDe:(NSString *)longitudE {
    

    self = [super init];
    if (self) {
        
        hNumber = hNo;
        hName = hNme;
        hArea = hAra;
        hBeds = hBds;
        hCode = hCde;
        price = pRce;
        imgUR0= imageUR0;
        imgUR1 = imgur1;
        imgUR2 = imgur2;
        imgUR3 = imgur3;
        hDesc = descrip;
        latiDe = latitudE;
        longDe = longitudE;
        
    }
    return self;
}
//LOAD IMAGES FROM URL AND COVERT TO DATA
-(void)loadData {
    
    NSURL *url = [NSURL URLWithString:self.imgUR0];
    self.imgData = [NSData dataWithContentsOfURL:url];
    NSURL *url1 = [NSURL URLWithString:self.imgUR1];
    self.imgData1 = [NSData dataWithContentsOfURL:url1];
    NSURL *url2 = [NSURL URLWithString:self.imgUR2];
    self.imgData2 = [NSData dataWithContentsOfURL:url2];
    NSURL *url3 = [NSURL URLWithString:self.imgUR3];
    self.imgData3 =[NSData dataWithContentsOfURL:url3];
}
-(id) initWithtextInputGlobalString: (NSString *) textInGloStr {
    
    self = [super init];
    if (self) {
        textInputGlobalString=textInGloStr;
        NSLog(@"the textfield object:%@",textInputGlobalString);
    }
    return self;
}
-(id) initWithsaleType:(NSString *)saleTypeStr {
    
    self = [super init];
    if (self) {
        saleType = saleTypeStr;
         NSLog(@"the saletype:%@",saleType);
        
    }
    return self;
    
}
@end
