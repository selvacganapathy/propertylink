//
//  propertyTable.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "propertyObject.h"
#import <QuartzCore/QuartzCore.h>
#import "searchView.h"
@interface propertyTable : UITableViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate>
@property(weak,nonatomic) NSString *introString;
@property(weak,nonatomic) NSString *saleType2;


//TABLEVIEW PROPERTIES
@property(retain,nonatomic) IBOutlet UITextField *textfieldarea;
@property(nonatomic,strong) NSMutableArray *jSonArray;
@property(nonatomic,strong) NSMutableArray *propertyArray;
@property(nonatomic,strong) IBOutlet UITableView *propertyListTableView;
@property(nonatomic,strong) IBOutlet UIToolbar *toolbar;
#pragma  - methods
//RETREIVE DATA FROM JSON
-(void) retreiveData;
-(IBAction)clickedButton1;
@property (strong,nonatomic) IBOutlet UILabel *textF;
@property (strong,nonatomic) IBOutlet UILabel *saleT;

@property (strong,nonatomic) NSString *textFstr;
@property (strong,nonatomic) NSString *saleTstr;
@end
