//
//  searchView.m
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 24/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import "searchView.h"
#import "propertyTable.h"
#import "propertyObject.h"
#import "AppDelegate.h"
#import "Latestsearch.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>


@interface searchView ()

@end

@implementation searchView {

    NSArray *miniTableArray; //ARRAY FOR TABLEVIEW
    
}
@synthesize tableViewMini,textFieldAray,textInputField,activityInd,initialLocation;
@synthesize theView,saleType1,passStrArray,saleType11,toRent,autoCompleteArray;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    saleType1.text = Nil;
    theView.layer.cornerRadius = 10.0;
    theView.layer.masksToBounds = YES;
    textInputField.delegate = self;
   
    
    [textInputField resignFirstResponder]; //RESIGN TEXTFIELD WHEN LOADING
    self.textInputField.tintColor = [UIColor purpleColor];//ASSIGN COLOR TO UITEXTFIELD
    AppDelegate *appDeleg = [[UIApplication sharedApplication]delegate]; //INITITTE CORE DATA
    _managedObjectContext = [appDeleg managedObjectContext]; //APPLY MANAGED OBJECTS
     self.tableViewMini.tableFooterView = [[UIView alloc]init]; //HIDE EMTY CELLS
    miniTableArray = [NSArray arrayWithObjects:@"Favorities",@"Find an Office", nil]; //ARRAY OBJECTS
    self.navigationController.toolbarHidden=YES;
    self.navigationController.toolbar.clipsToBounds = YES;
    UIImage *toolbarImage = [[UIImage imageNamed:@"search_BottomBar1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UIToolbar appearance]setBackgroundImage:toolbarImage forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    

    

}
-(void)viewDidAppear:(BOOL)animated {
    [self.activityInd stopAnimating];//STOP ANIMATION WHEN LOADING
    self.navigationController.toolbarHidden= YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)newtableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ICellIdentifier = @"DPcell";
    
    UITableViewCell *cell = [tableViewMini dequeueReusableCellWithIdentifier:ICellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ICellIdentifier];
        
    }
    cell.textLabel.text=[miniTableArray objectAtIndex:indexPath.row];
    cell.textLabel.font =  [UIFont systemFontOfSize:15.0];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     if (indexPath.row ==0) {//CALL FAVORITES VC
        UIViewController *favVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FavList"];
        [self.navigationController pushViewController:favVC animated:YES];
        
    }else if (indexPath.row ==1) {//CALL FIND AN OFFICE VC
        UIViewController *findOffice = [self.storyboard instantiateViewControllerWithIdentifier:@"findO"];
        [self.navigationController pushViewController:findOffice animated:YES];
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField { //NOT TO LOAD EMPTY ROWS IN THE LATEST SEARCH VC
   if ([textInputField.text length]>0) {
       NSDate *todayDate = [NSDate date];
       NSDateFormatter *dateFormat = [[NSDateFormatter alloc ]init];
       [dateFormat setDateFormat:@"MM/dd/yy hh:mm"];
       NSString *dateStr = [dateFormat stringFromDate:todayDate];
       NSLog(@"date:%@",dateStr);
       
        Latestsearch *searchContent = [NSEntityDescription insertNewObjectForEntityForName:@"Latestsearch" inManagedObjectContext:_managedObjectContext];
        [searchContent setSearch:textInputField.text];
        [searchContent setTime:dateStr];
        //SAVE THAT TO THE DATABASE
        NSError *error =Nil;
        if(![_managedObjectContext save:&error]) {
            //it is to handle the error
        }
           }
}
- (IBAction)Close:(id)sender { //CLICK TO HIDE THE KEYBOARD WHILE TOUCH ANYWHERE ON SCREEN
    [textInputField resignFirstResponder];
}

- (IBAction)toRent:(id) sender{
    saleType1.text = @"16";
    [activityInd startAnimating];
    [textInputField resignFirstResponder];
    [activityInd performSelector:@selector(stopAnimating) withObject:Nil afterDelay:2.0];
        
}
-(IBAction)tosale:(id)sender {
    saleType1.text = @"1";
    [activityInd startAnimating];
    [textInputField resignFirstResponder];
    [activityInd performSelector:@selector(stopAnimating) withObject:Nil afterDelay:2.0];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    propertyTable *transFerDetails = segue.destinationViewController;
    
    if ([segue.identifier isEqualToString:@"intro"]) {
        NSLog(@"1called");
        transFerDetails.saleType2 = @"16";
        transFerDetails.introString = [NSString stringWithFormat:@"%@",textInputField.text];
    }
    else if ([segue.identifier isEqualToString:@"duction"]) {
        NSLog(@"2 called");
        transFerDetails.saleType2 = @"1";
        transFerDetails.introString = [NSString stringWithFormat:@"%@",textInputField.text];
        

    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return  YES;
    
}
-(BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([textInputField.text isEqualToString:@""] && [identifier isEqualToString:@"intro"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Property Alert" message:@"Please Enter Your Area! " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }else if ([textInputField.text isEqualToString:@""] && [identifier isEqualToString:@"duction"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Property Alert" message:@"Please Enter Your Area! " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }else {
        return YES;
    }
    return YES;
}

-(IBAction)Animate:(id)sender{
    if(isAnimated) {
    }
    
    else{
        //view isnt animated to enlarge it
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Property Link Registration" message:@"Have you got your Property Details?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alertView show];
        
        [UIView beginAnimations:nil context:NULL];
        //[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.3];
        [theView setFrame:CGRectMake(10, 230, 300, 230)];
        [UIView commitAnimations];
        isAnimated=YES;
        theView.hidden=NO;
        
        //adding Cancellation button to subView
        UIImage *buttonImage = [UIImage imageNamed:@"cl2.png"];
        UIButton *canx = [UIButton buttonWithType:UIButtonTypeRoundedRect];
       
        MKMapView *mapView1 = [[MKMapView alloc]initWithFrame:CGRectMake(10, 10, 280,210)];
        mapView1.frame = CGRectMake(10, 10, 280, 210);
        mapView1.showsUserLocation =YES;
        mapView1.delegate = self;
        [mapView1 setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        mapView1.mapType = MKMapTypeStandard;
        mapView1.layer.cornerRadius= 10;
        mapView1.layer.masksToBounds =YES;
        theView.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0.9];
        theView.opaque = NO;
        [self.theView addSubview:mapView1];
        
        //callling the handle exit to close the button
        [canx addTarget:self action:@selector(handleExit) forControlEvents:UIControlEventTouchUpInside];
        [canx setFrame:CGRectMake(258, 8, 34 , 34)];
        [canx setBackgroundImage:buttonImage forState:UIControlStateNormal];
        //[canx setTitle:@"X" forState:UIControlStateNormal];
        [theView addSubview:canx];
    }
}

-(void) handleExit {
    //to set the animation to NO
    [UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [theView setFrame:CGRectMake(160, 324, 0,0)];
    [UIView commitAnimations];
    isAnimated=NO;
    
}

@end
