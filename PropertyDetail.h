//
//  PropertyDetail.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 09/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "propertyObject.h"
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>

@interface PropertyDetail : UIViewController<UIActionSheetDelegate,UIAlertViewDelegate,MKMapViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate,UIScrollViewDelegate> {
    BOOL isAnimated;
   
    
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollInDetails;
@property (strong, nonatomic) IBOutlet UIScrollView *imgScroll;
@property (strong, nonatomic) IBOutlet UIPageControl *pageContimages;
@property (nonatomic,retain) propertyObject *receP;
//LABEL AND IMAGES
@property (strong, nonatomic) IBOutlet UILabel *houseNumber;
@property (strong, nonatomic) IBOutlet UILabel *AddressLine;
@property (strong, nonatomic) IBOutlet UILabel *Postcode;
@property (strong, nonatomic) IBOutlet UILabel *Value;
@property (strong, nonatomic) IBOutlet UILabel *Viewing;

//@property (strong, nonatomic) IBOutlet UILabel *description1;
@property (strong, nonatomic) IBOutlet UILabel *county;
@property (nonatomic,retain) IBOutlet UIImageView *dragImg;
@property (strong, nonatomic) IBOutlet UITextView *description1;

//COREDATA
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (weak, nonatomic) IBOutlet UIView *theView;
- (IBAction)bookMark;;
-(IBAction)callNumber;
-(IBAction)emailTo;
-(IBAction)requestViewTo:(id)sender;
-(IBAction)zoomIn:(id)sender;
-(IBAction)zoomOut:(id)sender;

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
@property (nonatomic, strong) CLLocationManager *locationmanager;
@property (nonatomic, strong) IBOutlet UILabel *distancL;

@end
