//
//  searchView.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 24/11/2013.
//  Copyright (c) 2013 SCube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "propertyObject.h"
#import <MapKit/MapKit.h>


@interface searchView : UIViewController  <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,MKMapViewDelegate> {
    
     IBOutlet UITextField *textInputField;
     BOOL isAnimated;

}
@property(strong,nonatomic)  UITextField *saleType1;
@property(strong,nonatomic)  UITextField *saleType11;


@property(nonatomic,strong) IBOutlet UITextField *textInputField; //INPUT TEXRFIELD
@property(nonatomic,strong) NSMutableArray *textFieldAray; //INPUTTEXFIELDARRAYFORCOREDATA
@property(nonatomic,strong) NSMutableArray *passStrArray;
@property(nonatomic,strong) NSArray *autoCompleteArray;

@property (strong,nonatomic) IBOutlet UITableView *tableViewMini; //MINITABLEVIEW FOR LATEST,FAVORITES,OFFICE
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;//ROLLING CIRCLE FOR LOADING
//COREDATA
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic,strong) UIButton *toRent;

- (IBAction)toRent:(id)sender;
-(IBAction)tosale:(id)sender;
- (IBAction)Close:(id)sender;

-(IBAction)Animate:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *theView;
@property (nonatomic,retain) CLLocation *initialLocation;
@end
