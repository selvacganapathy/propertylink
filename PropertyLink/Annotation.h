//
//  Annotation.h
//  PropertyLink
//
//  Created by selvaganapathy chinnakalai on 14/01/2014.
//  Copyright (c) 2014 SCube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Annotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;


@end
